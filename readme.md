# Scrapy Depto

Este proyecto pretende extraer información básica sobre la compra y venta de departamento en Santiago (en primera instancia). La idea es recorrer con una araña todos los días paginas o portales de departamento para ir observando los departamento que están disponible en el mercado.

Paginas a scrapear:
* https://www.portalinmobiliario.com/
* https://www.goplaceit.com/cl/departamento/arriendo
* https://chilepropiedades.cl/propiedades/arriendo-mensual/departamento/santiago/0

__Estas paginas deberían ir creciendo__

Los datos a extraer son:
* Habitaciones
* Baño
* Ubicación
* Precio
* Superficie
* Año del departamento

# Para qué

Mi idea es tener una proyección (necesito muchos datos), de cuánto costaría un arriendo de un departamento a futuro. También que tan alejado esta el valor una propiedad con respecto a las de sus vecinos (mismo edificio, mismo barrio). Ver un mapa de color, de mas o menos los precios que se pueden llegar a encontrar. Ver un gráfico de lo varia los precios en ciertos edificios.

# Como hacerlo

* Lo primero que hay que hacer es sacar todos las publicaciones ya inscritas, TODAS, hasta la ultima, y guardar en un base de datos
* Limpiar los datos, siempre esta la posibilidad de que tengamos que limpiar algunos datos
* Exponer api para hacer más fácil el uso de datos
